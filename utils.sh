#/bin/bash

dir_exists() {
    local folder=$1
    if [ ! -d "${folder}" ]; then
        echo "Directory ${folder} does not exist"
        exit 1
    fi
}

file_exists() {
    local infile=$1
    if [ ! -f "${infile}" ]; then
        echo "${infile} file does not exist"
        exit 1
    fi
}

get_execution_time() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} command -- Return the execution time of a command (in seconds)"
        exit 1
    fi

    LC_NUMERIC=C  # to have a proper decimal-separator
    TIMEFORMAT="%R"  # to have the command time return the result in seconds

    local cmd=$1
    local dummy=$(time (eval ${cmd} 2> /dev/null) 2>&1)
    local duration=$(echo ${dummy} | tr -d  "'")  # remove trialing quotes
    echo "${duration}"
}

print_csv_header() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} command -- Return the CSV header"
        exit 1
    fi
    local csv_header_file=$1

    file_exists ${csv_header_file}
    local csv_header=$(<"${csv_header_file}")  # better than doing a 'cat'
    echo "${csv_header}"
}

