# Image Codec Bench

A script to run performance tests on various image compressors.

The script returns a CSV output with the following fields:
- the compressor fullname (name, version, level of compression and optionally the number of threads)
- the compression speed in MB/s
- the decompression speed in MB/s
- the input file size
- the compressed file size
- the compression ratio
- the filename

The supported compressors are: openjpeg, libjpeg.

# Requirements
 - libjpeg: https://github.com/thorfdbg/libjpeg
 - openjpeg: https://github.com/uclouvain/openjpeg
 - ffmpeg: https://ffmpeg.org/download.html

# Usage for image compression
The fastest way to obtain metrics on a set of images using lossless codecs is through `ffmpeg`. First, define the list of images that you want to compress/decompress in `config_ffmpeg_jpeg.sh`. For instance:
```
INFILES_PATTERN="tiff_seq_16b/tomo_ellisella_%04d.tiff"
```
Then to obtain the image codec performance metrics (saved in a `.csv` output file), run:
```
./run_ffmpeg_jpeg.sh -c jpegls > out.csv
```
or
```
./run_ffmpeg_jpeg.sh -c libopenjpeg > out.csv
```
More options are available:
```
./run_ffmpeg_jpeg.sh -h
```

However, if you do not want to use `ffmpeg`, but the original codec libraries instead, it is still possible. First, define the list of images that you want to compress/decompress in `config.sh`. For instance:
```
INFILES=$(ls -1 some_folder/image_*.tiff)
```
Then to obtain the image codec performance metrics (saved in a `.csv` output file), run either:
```
./run_libjpeg.sh > out.csv
```
or
```
./run_openjpeg.sh  > out.csv
```

# Usage for video compression
First, define the list of images that you want to compress/decompress in `config_ffmpeg_video.sh`. For instance:
```
INFILES_PATTERN="tiff_seq_16b/tomo_ellisella_%04d.tiff"
```
Then to obtain the image codec performance metrics (saved in a `.csv` output file), run:
```
./run_ffmpeg_video.sh -c libx264 > out.csv
```
or
```
./run_ffmpeg_jpeg.sh -c libx265 > out.csv
```
or
```
./run_ffmpeg_jpeg.sh -c ffv1 > out.csv
```
More options are available:
```
./run_ffmpeg_video.sh -h
```


# Additonal tools
Bash scripts to obtain the performance metrics using the bitstream codecs `lz4` and `zstd` to compare with image codecs:
```
./run_lz4.sh > out.csv
```
or
```
./run_zstd.sh > out.csv
```

Python script to convert an `.edf` file into a sequence of `.tiff` images (with some  `prefix`) saved in the directory `outdir`:
```
python3 convert_edf_to_tiff_seq.py infile.edf -o outdir -p prefix
```

Python script to compute the SSIM between two `.tiff` images:
```
python3 compute_ssim.py image1.tiff image2.tiff
```

Bash script to obtain a more readable report on the metrics based on a `.csv` output file:
```
./get_metrics_from_csv.sh out.csv 
```

