#/bin/bash

. config.sh
. options.sh
. metrics.sh

COMPRESSOR="jpeg"   # libjpeg
OPTIONS="-ls 0 -c"  # JPEG-LS
EXTENSION="jls"

COMPRESSOR_FULLNAME="${COMPRESSOR} ${OPTIONS}"

[ "${USE_HEADER}" = true ] && print_csv_header ${CSV_HEADER_FILE}

PGM_FILE="${OUTDIR}/toto.pgm"
PGM_FILE_D="${OUTDIR}/toto_d.pgm"
OUTFILE_C="${OUTDIR}/toto.${EXTENSION}"
OUTFILE_D="${OUTDIR}/toto.tif"

for INFILE in ${INFILES}; do
    
    tifftopnm ${INFILE} > ${PGM_FILE} 2> /dev/null
    
    CMD_C="${COMPRESSOR} ${OPTIONS} ${PGM_FILE} ${OUTFILE_C} &> /dev/null"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_C}"
    COMPRESSION_TIME=$(get_execution_time "${CMD_C}");
    [ "${DEBUG}" = true ] && echo "Compression time: ${COMPRESSION_TIME} second(s)"
    
    CMD_D="${COMPRESSOR} ${OUTFILE_C} ${PGM_FILE_D} &> /dev/null"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_D}"
    DECOMPRESSION_TIME=$(get_execution_time "${CMD_D}");
    [ "${DEBUG}" = true ] && echo "Decompression time: ${DECOMPRESSION_TIME} second(s)"

    pnmtotiff ${PGM_FILE_D} > ${OUTFILE_D}
    
    ORIGINAL_SIZE=$(get_file_size ${INFILE})
    COMPRESSED_SIZE=$(get_file_size ${OUTFILE_C})

    if [ "${CHECK_SSIM}" = true ]; then
        check_ssim ${INFILE} ${OUTFILE_D}
    fi
    
    print_metrics "${COMPRESSOR_FULLNAME}" "${ORIGINAL_SIZE}" "${COMPRESSED_SIZE}" "${COMPRESSION_TIME}" "${DECOMPRESSION_TIME}" "${INFILE}"

    rm -f ${PGM_FILE} ${OUTFILE_C} ${PGM_FILE_D} ${OUTFILE_D}
done

