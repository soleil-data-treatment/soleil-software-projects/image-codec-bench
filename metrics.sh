#/bin/bash

MIB_TO_BYTES=1048576
GIB_TO_BYTES=1073741824

SECOND_TO_MILLISECOND=1000

get_file_size() {
    if [  $# -lt 1 ]; then
        echo "Usage: $FUNCNAME infile -- Return the file size in bytes"
        exit 1
    fi
    
    local infile=$1
    local file_size=$(du -sb ${infile} | cut -f1)
    echo ${file_size}
}

get_filelist_size() {
    if [  $# -lt 1 ]; then
        echo "Usage: $FUNCNAME infiles -- Return the total size in bytes of a list of files"
        exit 1
    fi

    local infiles=$1
    local filelist_size=$(du -sbc ${infiles} | tail -n 1 | cut -f1)
    echo ${filelist_size}
}

get_dir_size() {
    if [  $# -lt 1 ]; then
        echo "Usage: $FUNCNAME indir -- Return the directory size in bytes"
        exit 1
    fi

    local indir=$1
    local dir_size=$(du -sb ${indir}/ | cut -f1)
    echo ${dir_size}
}

get_duration() {
    if [  $# -lt 2 ]; then
        echo "Usage: $FUNCNAME file_size speed - Return the de\compression time (in s)"
        exit 1
    fi

    local file_size=$1
    local speed=$2
    local duration=$(echo "scale=2; ${file_size} / (${speed} * ${MIB_TO_BYTES})" | bc -l)  # in s
    echo ${duration}
}

get_speed() {
    if [  $# -lt 2 ]; then
        echo "Usage: $FUNCNAME file_size duration - Return the de\compression speed (in MB/s)"
        exit 1
    fi

    local file_size=$1
    local duration=$2
    local speed=$(echo "scale=2; ${file_size} / ${MIB_TO_BYTES} / ${duration}" | bc -l)  # in MB/s
    echo ${speed}
}

get_ratio() {
    if [  $# -lt 2 ]; then
        echo "Usage: $FUNCNAME original_file_size compressed_file_size -- Return the compression ratio"
        exit 1
    fi

    local original_file_size=$1
    local compressed_file_size=$2
    local ratio=$(echo "scale=2; ${original_file_size} / ${compressed_file_size}" | bc -l)
    echo ${ratio}
}

print_metrics() {
    if [  $# -lt 6 ]; then
        echo "Usage: $FUNCNAME compressor_name original_file_size compressed_file_size time_to_compress time_to_decompress infile"
        exit 1
    fi

    local compressor_name=$1
    local size_raw=$2
    local size_c=$3
    local time_c=$4
    local time_d=$5
    local infile=$6

    local speed_c=$(get_speed "${size_raw}" "${time_c}")
    local speed_d=$(get_speed "${size_raw}" "${time_d}")
    local comp_ratio=$(get_ratio "${size_raw}" "${size_c}")
 
    echo "${compressor_name},${speed_c},${speed_d},${size_raw},${size_c},${comp_ratio},${infile}"
}

check_ssim() {
    if [  $# -lt 2 ]; then
        echo "Usage: ${FUNCNAME} file1 file2 -- Check that the SSIM is equal to 1"
        exit 1
    fi
    local file1=$1
    local file2=$2
    
    >&2 echo "Checking SSIM..."
    { ffmpeg -i ${file1} -i ${file2} -filter_complex ssim -f null - |& grep -Po "SSIM .*" | grep "All:1.000000"; } &> /dev/null
    if [  $? -ne 0 ]; then
        echo "SSIM is not equal to 1!"
        exit 1
    fi
}

