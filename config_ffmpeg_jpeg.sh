#/bin/bash

INFILES_PATTERN="tiff_seq_16b/tomo_ellisella_%04d.tiff"
#INFILES_PATTERN="tiff_seq_16b/tomo_ellisella_000%01d.tiff"

CSV_HEADER_FILE="CSV_HEADER.txt"

DEFAULT_CODEC="jpegls"
CODEC_LIST=("libopenjpeg" "jpegls")
