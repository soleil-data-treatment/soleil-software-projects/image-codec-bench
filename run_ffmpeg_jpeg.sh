#/bin/bash

. config_ffmpeg_jpeg.sh
. options.sh
. metrics.sh

if [ ${CODEC} == "libopenjpeg" ]; then
    EXTENSION="j2k"
elif [ ${CODEC} == "jpegls" ]; then
    EXTENSION="jls"
else
    echo "Could not find any extension for this codec"
    exit 1
fi

OPTIONS=
if [ -n "${THREAD}" ]; then
    OPTIONS="-threads ${THREAD}"
fi

if test -z "${OPTIONS}"; then
    COMPRESSOR_FULLNAME="ffmpeg -vcodec ${CODEC}"
else
    COMPRESSOR_FULLNAME="ffmpeg -vcodec ${CODEC} ${OPTIONS}"
fi

INDIR=${INFILES_PATTERN%/*}
FILENAME_PATTERN=${INFILES_PATTERN##*/}

OUTDIR_C="${OUTDIR}/co"
OUTDIR_D="${OUTDIR}/dec"
mkdir -p ${OUTDIR_C} ${OUTDIR_D}

OUTFILES_PATTERN_C="${OUTDIR_C}/${FILENAME_PATTERN%.tiff}.${EXTENSION}"
OUTFILES_PATTERN_D="${OUTDIR_D}/${FILENAME_PATTERN}"

CMD_C="ffmpeg -i ${INFILES_PATTERN} -vcodec ${CODEC} -start_number 0 ${OPTIONS} ${OUTFILES_PATTERN_C}"
[ "${DEBUG}" = true ] && echo "Will run: ${CMD_C}"
COMPRESSION_TIME=$(get_execution_time "${CMD_C}");
[ "${DEBUG}" = true ] && echo "Compression time: ${COMPRESSION_TIME} second(s)"
    
CMD_D="ffmpeg -i ${OUTFILES_PATTERN_C} -vcodec ${CODEC} -start_number 0 ${OPTIONS} ${OUTFILES_PATTERN_D}"
[ "${DEBUG}" = true ] && echo "Will run: ${CMD_D}"
DECOMPRESSION_TIME=$(get_execution_time "${CMD_D}");
[ "${DEBUG}" = true ] && echo "Decompression time: ${DECOMPRESSION_TIME} second(s)"

ORIGINAL_SIZE=$(get_filelist_size "${INDIR}/*.tiff")
COMPRESSED_SIZE=$(get_filelist_size "${OUTDIR_C}/*.${EXTENSION}")

[ "${USE_HEADER}" = true ] && print_csv_header ${CSV_HEADER_FILE}
print_metrics "${COMPRESSOR_FULLNAME}" "${ORIGINAL_SIZE}" "${COMPRESSED_SIZE}" "${COMPRESSION_TIME}" "${DECOMPRESSION_TIME}" "${INDIR}"

# Check the SSIM on individual files
if [ "${CHECK_SSIM}" = true ]; then
   for f in $(ls -1 ${OUTDIR_D}/*.tiff); do
      fname=${f##*/}
      [ "${DEBUG}" = true ] && echo "- ${fname}"
      check_ssim $f ${INDIR}/${fname}
   done
fi

