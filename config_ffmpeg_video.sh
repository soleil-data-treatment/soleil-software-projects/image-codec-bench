#/bin/bash

INFILES_PATTERN="tiff_seq_16b/tomo_ellisella_%04d.tiff"
#INFILES_PATTERN="tiff_seq_16b/tomo_ellisella_000%01d.tiff"

CSV_HEADER_FILE="CSV_HEADER.txt"

DEFAULT_CODEC="ffv1"
CODEC_LIST=("libx264" "libx265" "ffv1")

PRESET="ultrafast"
BITS_PER_RAW_SAMPLE="16"
