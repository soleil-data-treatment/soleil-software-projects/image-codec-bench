#/bin/bash

. config.sh
. options.sh
. metrics.sh

COMPRESSOR="zstd"
OPTIONS="-${LEVEL}"
if [ -n "${THREAD}" ]; then
    OPTIONS="-${LEVEL} -T${THREAD}"
fi
EXTENSION="zst"

VERSION=$(${COMPRESSOR} -V | grep -oP '(?<=v)\K.*(?=,)')
COMPRESSOR_FULLNAME="${COMPRESSOR} ${VERSION} ${OPTIONS}"

[ "${USE_HEADER}" = true ] && print_csv_header ${CSV_HEADER_FILE}

OUTFILE_C="${OUTDIR}/toto.tif.${EXTENSION}"
OUTFILE_D="${OUTDIR}/toto.tif"

for INFILE in ${INFILES}; do
    ORIGINAL_SIZE=$(get_file_size ${INFILE})
    
    CMD_C="${COMPRESSOR} ${OPTIONS} ${INFILE} -o ${OUTFILE_C} -f -q"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_C}"
    COMPRESSION_TIME=$(get_execution_time "${CMD_C}");
    [ "${DEBUG}" = true ] && echo "Compression time: ${COMPRESSION_TIME} second(s)"
    
    COMPRESSED_SIZE=$(get_file_size ${OUTFILE_C})
    
    CMD_D="${COMPRESSOR} -d ${OUTFILE_C} -o ${OUTFILE_D} -f -q"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_D}"
    DECOMPRESSION_TIME=$(get_execution_time "${CMD_D}");
    [ "${DEBUG}" = true ] && echo "Decompression time: ${DECOMPRESSION_TIME} second(s)"

    print_metrics "${COMPRESSOR_FULLNAME}" "${ORIGINAL_SIZE}" "${COMPRESSED_SIZE}" "${COMPRESSION_TIME}" "${DECOMPRESSION_TIME}" "${INFILE}"

    rm -f ${OUTFILE_C} ${OUTFILE_D}
done

