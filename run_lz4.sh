#/bin/bash

. config.sh
. options.sh
. metrics.sh

COMPRESSOR="lz4"
OPTIONS="-${LEVEL}"
EXTENSION="lz4"

VERSION=$(${COMPRESSOR} -V | grep -oP '(?<=v)\K.*(?=,)')
COMPRESSOR_FULLNAME="${COMPRESSOR} ${VERSION} ${OPTIONS}"

[ "${USE_HEADER}" = true ] && print_csv_header ${CSV_HEADER_FILE}

OUTFILE_C="${OUTDIR}/toto.tif.${EXTENSION}"
OUTFILE_D="${OUTDIR}/toto.tif"

for INFILE in ${INFILES}; do
    
    CMD_C="${COMPRESSOR} ${OPTIONS} -f -q -k ${INFILE} ${OUTFILE_C}"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_C}"
    COMPRESSION_TIME=$(get_execution_time "${CMD_C}");
    [ "${DEBUG}" = true ] && echo "Compression time: ${COMPRESSION_TIME} second(s)"

    CMD_D="${COMPRESSOR} -f -q -k -d ${OUTFILE_C} ${OUTFILE_D}"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_D}"
    DECOMPRESSION_TIME=$(get_execution_time "${CMD_D}");
    [ "${DEBUG}" = true ] && echo "Decompression time: ${DECOMPRESSION_TIME} second(s)"

    ORIGINAL_SIZE=$(get_file_size ${INFILE})
    COMPRESSED_SIZE=$(get_file_size ${OUTFILE_C})

    print_metrics "${COMPRESSOR_FULLNAME}" "${ORIGINAL_SIZE}" "${COMPRESSED_SIZE}" "${COMPRESSION_TIME}" "${DECOMPRESSION_TIME}" "${INFILE}"
    
    rm -f ${OUTFILE_C} ${OUTFILE_D}
done

