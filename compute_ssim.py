#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import numpy as np
from skimage import io
from skimage.metrics import structural_similarity as ssim

def get_first_channel(img):
    """Get the first dimension of a three-channel images"""
    dim = len(img.shape)
    if dim == 3:
        return img[:,:,0]
    return img


def main(gray_img1, gray_img2):
    """Compute SSIM between two images"""
    score, diff = ssim(gray_img1, gray_img2, full=True)
    print("SSIM: {}".format(score)) 
    diff = gray_img1 - gray_img2
    print("RMS(diff): {}".format(np.sqrt(np.mean(diff**2))))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('first_tiff_file', type=str, help="Input tiff file #1")
    parser.add_argument('second_tiff_file', type=str, help="Input tiff file #2")

    args = parser.parse_args()
    
    img1 = io.imread(args.first_tiff_file)
    img2 = io.imread(args.second_tiff_file)

    img1 = get_first_channel(img1)
    img2 = get_first_channel(img2)
    main(img1, img2)

