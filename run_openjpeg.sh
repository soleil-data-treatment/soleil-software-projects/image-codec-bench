#/bin/bash

. config.sh
. options.sh
. metrics.sh

COMPRESSOR="openjpeg"  # JPEG2000 lossless
OPTIONS=
if [ -n "${THREAD}" ]; then
    OPTIONS="-threads ${THREAD}"
fi
EXTENSION="j2k"

VERSION=$(opj_compress -h | grep "library" | grep -oP '(\d+\.)+.')
if test -z "${OPTIONS}"; then
   COMPRESSOR_FULLNAME="${COMPRESSOR} ${VERSION}"
else
   COMPRESSOR_FULLNAME="${COMPRESSOR} ${VERSION} ${OPTIONS}"
fi

[ "${USE_HEADER}" = true ] && print_csv_header ${CSV_HEADER_FILE}

PGM_FILE="${OUTDIR}/toto.pgm"
OUTFILE_C="${OUTDIR}/toto.${EXTENSION}"
OUTFILE_D="${OUTDIR}/toto.tif"

for INFILE in ${INFILES}; do
    
    tifftopnm ${INFILE} > ${PGM_FILE} 2> /dev/null
    
    # COMPRESSION_TIME_MS=$(opj_compress -i ${PGM_FILE} -o ${OUTFILE_C} ${OPTIONS} |& grep "encode time" | cut -d" "  -f3)
    # COMPRESSION_TIME=$(echo "scale=2; ${COMPRESSION_TIME_MS} / ${SECOND_TO_MILLISECOND}" | bc -l)

    CMD_C="opj_compress -i ${PGM_FILE} -o ${OUTFILE_C} ${OPTIONS} &> /dev/null"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_C}"
    COMPRESSION_TIME=$(get_execution_time "${CMD_C}");
    [ "${DEBUG}" = true ] && echo "Compression time: ${COMPRESSION_TIME} second(s)"
       
    # DECOMPRESSION_TIME_MS=$(opj_decompress -i ${OUTFILE_C} -o ${OUTFILE_D} ${OPTIONS} |& grep "decode time" | cut -d" "  -f3)
    # DECOMPRESSION_TIME=$(echo "scale=2; ${DECOMPRESSION_TIME_MS} / ${SECOND_TO_MILLISECOND}" | bc -l)       

    CMD_D="opj_decompress -i ${OUTFILE_C} -o ${OUTFILE_D} ${OPTIONS} &> /dev/null"
    [ "${DEBUG}" = true ] && echo "Will run: ${CMD_D}"
    DECOMPRESSION_TIME=$(get_execution_time "${CMD_D}");
    [ "${DEBUG}" = true ] && echo "Decompression time: ${DECOMPRESSION_TIME} second(s)"

    ORIGINAL_SIZE=$(get_file_size ${INFILE})
    COMPRESSED_SIZE=$(get_file_size ${OUTFILE_C})

    if [ "${CHECK_SSIM}" = true ]; then
        check_ssim ${INFILE} ${OUTFILE_D}
    fi

    print_metrics "${COMPRESSOR_FULLNAME}" "${ORIGINAL_SIZE}" "${COMPRESSED_SIZE}" "${COMPRESSION_TIME}" "${DECOMPRESSION_TIME}" "${INFILE}"

    rm -f ${PGM_FILE} ${OUTFILE_C} ${OUTFILE_D}
done

