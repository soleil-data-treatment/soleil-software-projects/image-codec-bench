#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import argparse
import os
import fabio
  
DEFAULT_PREFIX = 'toto'
DEFAULT_OUTDIR = '/tmp'
IMAGE_FORMAT = 'tiff'
 
 
def convert_edf_to_tiff_seq(edf_file, outdir=DEFAULT_OUTDIR, prefix=DEFAULT_PREFIX, frame_ids=None):
    """Convert and edf file containing images to a sequence of TIFF images"""
    fd = fabio.open(edf_file)
    nb_frames = fd.nframes
    print("Number of frames: {}".format(nb_frames))
  
    if not frame_ids:
        frame_ids = range(0, fd.nframes)
 
    for i in frame_ids:
        frame = fd.getframe(i)
        outfile = outdir + os.path.sep + "{}_{:04d}.{}".format(prefix, i, IMAGE_FORMAT)
        print(i, outfile)
        frame.convert(IMAGE_FORMAT).save(outfile)
 
 
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('edf_file', type=str, help="Input edf file to be converted to a sequence of image files")
    parser.add_argument('-i', '--frame-ids', type=int, nargs='+', default=None, help="List of wanted frame ids (default: all frames)")
    parser.add_argument('-o', '--outdir', type=str, default=DEFAULT_OUTDIR, help="Output directory where the sequence of images files will be stored")
    parser.add_argument('-p', '--prefix', type=str, default=DEFAULT_PREFIX, help="Prefix to be used for the output images files")
    args = parser.parse_args()
     
    convert_edf_to_tiff_seq(args.edf_file, frame_ids=args.frame_ids, outdir=args.outdir, prefix=args.prefix)


