#/bin/bash

. utils.sh

CODEC=
if [ -n "${DEFAULT_CODEC}" ]; then
    CODEC="${DEFAULT_CODEC}"
fi
OUTDIR=
USE_MKTEMP=false
CHECK_SSIM=false
USE_HEADER=true
DEBUG=false
LEVEL=1
THREAD=

cleanup() {
    EXIT_CODE=$?
    if [ "${USE_MKTEMP}" = true ]; then
        [ "${DEBUG}" = true ] && echo "Removing directory: ${OUTDIR}"
        rm -dfr ${OUTDIR}
    fi
    exit ${EXIT_CODE}
}
trap cleanup EXIT

usage() {
    echo "Usage: $0 [OPTIONS]"
    if [ -n "${CODEC}" ]; then
        echo " [ -c CODEC ] -- Codec to be used [default: ${CODEC}]"
        echo "   Available codecs: ${CODEC_LIST[@]}"
    fi
    echo " [ -o OUTDIR ]  -- Output directory were the compressed and decompressed images will be written [default: mktemp]"
    echo " [ -s ] -- Check SSIM"
    echo " [ -n ] -- No CSV header"
    echo " [ -d ] -- Debug mode"
    echo " [ -l LEVEL ]  -- Compression level [default: ${LEVEL}]"
    echo " [ -T THREAD ] -- Number of threads [default: none]"
}
         
while getopts "hc:dl:no:sT:" flag; do
    case ${flag} in
        h) 
            usage
            exit 0
            ;;
        c)
            CODEC=${OPTARG}
            ;;
        d)
            DEBUG=true
            ;;
        l)
            LEVEL=${OPTARG}
            ;;
        n)
            USE_HEADER=false
            ;;
        o)
            OUTDIR=${OPTARG}
            ;;
        s)
            CHECK_SSIM=true
            ;;
        T)
            THREAD=${OPTARG}
            ;;
        :)
            echo "-${OPTARG} requires an argument."
            exit 1
            ;;
        \?) 
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
    esac
done

shift $(( ${OPTIND} - 1 ))

if [ -z "${OUTDIR}" ]; then
    OUTDIR=$(mktemp -d)
    USE_MKTEMP=true
else
    dir_exists ${OUTDIR}
fi

[ "${DEBUG}" = true ] && echo "CODEC: ${CODEC}"
[ "${DEBUG}" = true ] && echo "OUTDIR: ${OUTDIR}"
[ "${DEBUG}" = true ] && echo "CHECK_SSIM: ${CHECK_SSIM}"
[ "${DEBUG}" = true ] && echo "USE_MKTEMP: ${USE_MKTEMP}"
[ "${DEBUG}" = true ] && echo "USE_HEADER: ${USE_HEADER}"
[ "${DEBUG}" = true ] && echo "DEBUG: ${DEBUG}"
[ "${DEBUG}" = true ] && echo "LEVEL: ${LEVEL}"
[ "${DEBUG}" = true ] && echo "THREAD: ${THREAD}"

if [ -n "${CODEC}" ]; then
    if [[ ! " ${CODEC_LIST[*]} " =~ " ${CODEC} " ]]; then
        # Extra white spaces needed for checking exact match!
        echo "Unknown codec: ${CODEC}"
        echo "The list of available codecs is: ${CODEC_LIST[@]}"
        exit 1
    fi
fi
