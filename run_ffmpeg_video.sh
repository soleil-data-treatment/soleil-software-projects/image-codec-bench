#/bin/bash

. config_ffmpeg_video.sh
. options.sh
. metrics.sh

ADDITONAL_OPTIONS_C=
ADDITONAL_OPTIONS_D=
if [ ${CODEC} == "libx264" ]; then
    EXTENSION="mp4"
    ADDITONAL_OPTIONS_C="-preset ${PRESET} -qp 0"
    ADDITONAL_OPTIONS_D="-pix_fmt gray16le"
elif [ ${CODEC} == "libx265" ]; then
    EXTENSION="mp4"
    ADDITONAL_OPTIONS_C="-preset ${PRESET} -qp 0 -x265-params lossless=1"
elif [ ${CODEC} == "ffv1" ]; then
    EXTENSION="avi"
    ADDITONAL_OPTIONS_C="-bits_per_raw_sample ${BITS_PER_RAW_SAMPLE}"
else
    echo "Could not find any extension for this codec"
    exit 1
fi

COMPRESSOR_FULLNAME="ffmpeg -vcodec ${CODEC} ${ADDITONAL_OPTIONS_C}"

INDIR=${INFILES_PATTERN%/*}
FILENAME_PATTERN=${INFILES_PATTERN##*/}

OUTDIR_C="${OUTDIR}/co"
OUTDIR_D="${OUTDIR}/dec"
mkdir -p ${OUTDIR_C} ${OUTDIR_D}

OUTFILE_C="${OUTDIR_C}/vid_${CODEC}_${PRESET}.${EXTENSION}"
OUTFILES_PATTERN_D="${OUTDIR_D}/${FILENAME_PATTERN}"

CMD_C="ffmpeg -i ${INFILES_PATTERN} -c:v ${CODEC} ${ADDITONAL_OPTIONS_C} ${OUTFILE_C}"
[ "${DEBUG}" = true ] && echo "Will run: ${CMD_C}"
COMPRESSION_TIME=$(get_execution_time "${CMD_C}");
[ "${DEBUG}" = true ] && echo "Compression time: ${COMPRESSION_TIME} second(s)"


CMD_D="ffmpeg -i ${OUTFILE_C} -start_number 0 ${ADDITONAL_OPTIONS_D} ${OUTFILES_PATTERN_D}"
[ "${DEBUG}" = true ] && echo "Will run: ${CMD_D}"
DECOMPRESSION_TIME=$(get_execution_time "${CMD_D}");
[ "${DEBUG}" = true ] && echo "Decompression time: ${DECOMPRESSION_TIME} second(s)"

ORIGINAL_SIZE=$(get_filelist_size "${INDIR}/*.tiff")
COMPRESSED_SIZE=$(get_file_size "${OUTFILE_C}")

[ "${USE_HEADER}" = true ] && print_csv_header ${CSV_HEADER_FILE}
print_metrics "${COMPRESSOR_FULLNAME}" "${ORIGINAL_SIZE}" "${COMPRESSED_SIZE}" "${COMPRESSION_TIME}" "${DECOMPRESSION_TIME}" "${INDIR}"

# Check the SSIM on individual files
if [ "${CHECK_SSIM}" = true ]; then
   check_ssim "${INFILES_PATTERN}" "OUTFILES_PATTERN_D"
fi
