#/bin/bash

CSVFILE=$1

. utils.sh
. metrics.sh

usage() {
	echo "Usage: $0 CSVFILE -- Return tc, td, tc+td and R based on the output CSV file containg the benchmark results"
    exit 1
}


main() {
    local infile=$1

    echo "compressor, Tc[s], Td[s], Tc+Td[s], Vc[MB/s], Vd'[MB/s], Vd[MB/s], N[MB], Nc[MB], R, datafile"
	{
        read  # skip first line (header)
        while IFS="," read -r compressor speed_c speed_d size_raw_bytes size_c_bytes ratio datafile; do
            [[ "${compressor}" =~ ^#.*$ ]] && continue
            [[ -z ${compressor} ]] && continue
            time_c=$(get_duration ${size_raw_bytes} ${speed_c})
            time_d=$(get_duration ${size_c_bytes} ${speed_d})
            time_cycle=$(echo "${time_c} + ${time_d}" | bc -l)
            speed_d_prime=$(echo "scale=2; ${speed_d} / ${ratio}" | bc -l)
            size_raw=$(echo "scale=1; ${size_raw_bytes}/ ${MIB_TO_BYTES}" | bc -l)  # in MB
            size_c=$(echo "scale=1; ${size_c_bytes} / ${MIB_TO_BYTES}" | bc -l)  # in MB
            echo "${compressor}, ${time_c}, ${time_d}, ${time_cycle}, ${speed_c}, ${speed_d_prime}, ${speed_d}, ${size_raw}, ${size_c}, ${ratio}, ${datafile}"
        done 
	} < "${infile}"
}



if [  $# -lt 1 ]; then
    usage
elif [ $# -ge 1 ]; then
    file_exists ${CSVFILE}
    main ${CSVFILE}
fi

exit 0

